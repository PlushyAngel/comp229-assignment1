import java.awt.*;
import java.util.Random;

public class PassCell {

    private int x;
    private int y;
    Color myColor;
    Random rand = new Random();

    public PassCell(int x, int y) {
        this.x = x;
        this.y = y;

        int randomColor = rand.nextInt(4);

        switch (randomColor) {
            case 0:
                myColor = new Color(102, 51, 0);
                break;
            case 1:
                myColor = new Color(102, 153, 0);
                break;
            case 2:
                myColor = new Color(0, 102, 0);
                break;
            case 3:
                myColor = new Color(182, 182, 182);
                break;
            default:
                myColor = Color.WHITE;
                break;
        }
    }

    public void paint(Graphics g, Boolean highlighted) {
        g.setColor(myColor);
        g.fillRect(x, y,35,35);
        if (highlighted) {
            g.setColor(Color.BLACK);
            g.drawRect(x + 1,y + 1, 33, 33);
        }
        g.setColor(Color.BLACK);
        g.drawRect(x, y, 35, 35);
    }

    public boolean contains(Point target){
        if (target == null) {
            return false;
        }
        return target.x > x && target.x < x + 35 && target.y > y && target.y < y +35;
    }
}