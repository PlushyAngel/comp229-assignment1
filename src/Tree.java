import java.awt.*;

public class Tree extends Cell {

    public Tree(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g, Boolean highlighted){
        g.setColor(new Color(0, 102, 0));
        g.fillRect(x, y,35,35);
        super.paint(g, highlighted);
    }
}