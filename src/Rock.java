import java.awt.*;

public class Rock extends Cell {

    public Rock(int x, int y) {
        super(x, y);
    }

    @Override
    public void paint(Graphics g, Boolean highlighted){
        g.setColor(new Color(182, 182, 182));
        g.fillRect(x, y,35,35);
        super.paint(g, highlighted);
    }
}
