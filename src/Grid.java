import java.awt.*;
import java.util.Random;

public class Grid {

    private Cell[][] grid  = new Cell[20][20];
    private Random rand = new Random();

    public Grid(int x, int y) {
        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 20; j++) {
                int randomColor = rand.nextInt(4);
                switch (randomColor) {
                    case 0:
                        grid[i][j] = new Dirt(x + j * 35, y + i * 35);
                        break;
                    case 1:
                        grid[i][j] = new Grass(x + j * 35,  y + i * 35);
                        break;
                    case 2:
                        grid[i][j] = new Tree(x + j * 35, y + i * 35);
                        break;
                    case 3:
                        grid[i][j] = new Rock(x + j * 35, y + i * 35);
                        break;
                }

            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        for(int y = 0; y < 20; y++) {
            for(int x = 0; x < 20; x++) {
                grid[x][y].paint(g, grid[x][y].contains(mousePosition));
            }
        }
    }
}